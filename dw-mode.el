;;; dw-mode.el --- Major mode for editing MuleSoft DataWeave files

;; This version supports only one output buffer for each Dataweave buffer

(require 'js)
(require 'mailcap)                      ;;mailcap-extension-to-mime

;;* Customization

(defgroup dataweave nil
  "Support for editing MuleSoft DataWeave files."
  :group 'languages
  :prefix "dw-")

(defface dw-output-field
  '((t (:inherit outline-1)))           ;;font-lock-function-name-face
  "Face used for the keys in output maps/objects."
  :group 'dataweave)

(defface dw-input-variable
  '((t (:inherit (dw-variable))))
  "Face used for variables used as input."
  :group 'dataweave)

(defface dw-variable
  '((t (:inherit outline-2)))           ;;font-lock-variable-name-face
  "Face used for variables not used as input."
  :group 'dataweave)

(defface dw-important-operator
  '((t (:inherit outline-3)))           ;;font-lock-constant-face
  "Face used for operators found in `dw-important-operators`."
  :group 'dataweave)

(defface dw-selector-key
  '((t (:inherit outline-5)))           ;;font-lock-type-face
  "Face used for the key names in selector expressions."
  :group 'dataweave)

(defvar dw-important-operators '("map" "when")
  "Operators to highlight with `dw-important-operator` face."
  ;; :type '(repeat string)
  ;; :group 'dataweave
  )

;; (defface dw-function-name
;;   '((t (:inherit (outline-6 font-lock-constant-face))))
;;   "Face used for function names."
;;   :group 'dataweave)

(defface dw-type
  '((t (:inherit outline-6)))           ;;font-lock-constant-face
  "Face used for type names."
  :group 'dataweave)

(defface dw-directive
  '((t (:inherit outline-7)))           ;;font-lock-builtin-face
  "Face used for directives."
  :group 'dataweave)

(defface dw-header-separator ;; a grey line
  `((((supports stipple))
     :height 100
     :stipple (7 9 ,(string 0 0 0 0 0 0 0 127 127))
     :inherit window-divider)
    (t :inherit (window-divider mode-line-inactive)))
  "Face used for the header/body separator line."
  :group 'dataweave)



(defcustom dw-libs nil
  "Paths to Dataweave jars."
  :type '(set (directory))
  :group 'dataweave )

(defcustom dw-auto-run-idle 1
  "Idle time delay before `dw-auto-run-mode' runs a DataWeave script."
  :type 'number
  :group 'dataweave)



(defun dw-studio-base-path-p (file)
  "Tests if FILE looks like base path where Anypoint Studio is installed. Returns 'dir-ok for true."
  (and (file-directory-p file)
       (file-exists-p (concat file "/AnypointStudio.ini"))
       (file-exists-p (concat file "/plugins"))
       'dir-ok))

(defun dw--find-dw-libs ()
  (customize-push-and-save
     'dw-libs
     (let ((studio-base-path
            (or (locate-file "AnypointStudio" (mapcar 'expand-file-name '("~" "~/Applications" "~/bin")) nil 'dw-studio-base-path-p)
                (read-file-name "Anypoint Studio base path: " nil nil t nil 'dw-studio-base-path-p))))
       (or (mapcar 'file-name-directory
                   (last (sort (directory-files-recursively studio-base-path "mule-plugin-weave-.*\\.jar" t) 'string<)))
           (user-error "Could not find Dataweave jars in \"%s\"." studio-base-path)))))

(defun dw--runner-classpath ()
  "builds the classpath for runner"
  (let ((libs (or dw-libs (dw--find-dw-libs))) cp)
    (while libs
      (setq cp (nconc cp (directory-files (car libs) t "\\.jar$" t)))
      (setq libs (cdr libs)))
    (mapconcat 'identity cp ":")))


(defconst dw--rx-body-begin "^---\n")

(defconst dw-mode-syntax-table
  (let ((table (make-syntax-table)))
    (c-populate-syntax-table table)
    (modify-syntax-entry ?= "." table)
    (modify-syntax-entry ?$ "_" table)
    (modify-syntax-entry ?` "\"" table)
    table)
  "Syntax table for `dw-mode`.") ;; \n for whole line

(defvar dw-font-lock-keywords
  `((,(rx (or bol "<![CDATA[") "%dw" (+ space) (+ digit) "." (+ digit)) . 'dw-directive)
    ("^%\\(\\sw\\|\\s_\\)+" . 'dw-directive)
    (,dw--rx-body-begin . 'dw-header-separator)
    (,(rx (or bol (in "({[,")) (* whitespace) (group (+ (or (syntax word) (syntax symbol)))) (* whitespace) (group ":") (or whitespace eol (syntax comment-start)))
     (1 'dw-output-field))
    (,(rx (group symbol-start "as") (+ space)  (group ":" (+ (or (syntax word) (syntax symbol)))))
      (1 font-lock-builtin-face)
      (2 'dw-type))
    (,(regexp-opt '("true" "false" "null") 'symbols) . font-lock-constant-face)
    (,(regexp-opt '("$" "$$") 'symbols) . font-lock-builtin-face)
    (,(regexp-opt dw-important-operators 'symbols) . 'dw-important-operator)
    (,(rx "." (? (in ".*@")) (* whitespace) symbol-start (group  (*? any)) symbol-end) (1 'dw-selector-key))
    (dw--search-forward-input-names . 'dw-input-variable)
    ;; (,(rx symbol-start "using" symbol-end  (* whitespace) "(" (* whitespace) symbol-start (group  (*? any)) symbol-end) (1 'font-lock-variable-name-face))
    ))

(defun dw--search-forward-input-names (bound)
  (when dw-runner-input-files
    (re-search-forward (regexp-opt (mapcar 'car dw-runner-input-files) 'symbols) bound t)))

(defvar dw-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-.") 'dw-goto-symbol)
    (define-key map (kbd "F11") 'dw-run)
    (define-key map (kbd "C-c C-r") 'dw-run)
    (define-key map (kbd "C-c C-c") 'dw-run)
    (define-key map (kbd "C-x C-e") 'dw-eval-dwim)
    (define-key map (kbd "C-c C-i") 'dw-set-input)
    map)
  "Keymap for `dw-mode'.")


;;;###autoload
(define-derived-mode dw-mode prog-mode "DataWeave"
  "Mode to edit MuleSoft DataWeave files.

\\{dw-mode-map}"
  :group 'dataweave
  :syntax-table dw-mode-syntax-table

  (setq-local indent-line-function 'dw-indent-line)
  (setq-local open-paren-in-column-0-is-defun-start nil)

  (setq-local comment-start "// ")
  (setq-local comment-start-skip "// *")

  (setq-local parse-sexp-ignore-comments t)
  (setq-local parse-sexp-lookup-properties t)

  (setq font-lock-defaults '(dw-font-lock-keywords))

  (setq dw-runner-input-files '())

  (add-hook 'after-save-hook 'dw--run-maybe)

  (add-to-list 'flycheck-checkers 'dataweave)
  (flycheck-mode t))

(defun dw--run-maybe ()
  "Calls `dw-run', but only if there is a live output buffer."
  (if (buffer-live-p dw-runner-output-buffer) (dw-run)))

(defun dw-in-body-p (&optional pos)
  "Is POS after header/body separator \"---\"."
  (save-excursion
    (setq pos (or pos (point)))
    (goto-char (point-min))
    (re-search-forward dw--rx-body-begin pos t)))

(defun dw-indent-line ()
  "Indent the current line as Dataweave."
  (interactive)
  ;; TODO: do not use js-indent-line
  (if (dw-in-body-p)
      ;; body
      (js-indent-line)
    ;; header
    (indent-line-to 0)
    ))

;;* Runner

(defvar-local dw-runner-input-files nil
  "An alist mapping input variable names to files to run with.")

(defvar-local dw-runner-output-buffer nil
  "Buffer in which to display output.")

(defun dw-payload (file)
  "Use FILE as 'payload'."
  (interactive "fpayload file: ")
  (dw-set-input "payload" file))

(defun dw--input-name-suggestions ()
  (save-excursion
    (goto-char (point-min))
    (re-search-forward (rx symbol-start (group (+ (or (syntax word) (syntax symbol)))) (* whitespace)))
    ))

(defun dw-set-input (name file)
  "Set the input variable NAME to FILE when running the script."
  (interactive
   (let* ((name (completing-read "input variable name: " dw-runner-input-files nil nil (thing-at-point 'symbol t)))
          (default (cdr (assoc-string name dw-runner-input-files)))
          (file (read-file-name (format  "`%s' file: " name) nil nil t default)))
     (while (file-directory-p file)
       (setq file (read-file-name (format  "`%s' FILE: " name) nil nil t default)))
     (list name file)))
  (setq dw-runner-input-files
        (cons (cons name file)
              (delq (assoc-string name dw-runner-input-files) dw-runner-input-files)))
  (font-lock-flush))

(defun dw-remove-input (name)
  "Remove an input variable NAME from being set when running the script."
  (interactive
   (list
    (completing-read "input variable name: " dw-runner-input-files nil t (thing-at-point 'symbol t))))
  (setq dw-runner-input-files
        (delq (assoc-string name dw-runner-input-files) dw-runner-input-files))
  (font-lock-flush))

(defun dw-goto-symbol (symbol)
  ;; TODO: goto other types of symbols
  (interactive (list (or (thing-at-point 'symbol t))))
  ;; (rx (group symbol-start (+? any) symbol-end) (* (seq (* space) "." (? (in ".*@")) symbol-start (*? any) symbol-end)))
  (let ((file (cdr (assoc-string symbol dw-runner-input-files))))
    (when (file-regular-p file)
      (xref-push-marker-stack)
      (find-file file))))
(defconst dw-mime-extensions
  '(("js" . "application/json")
    ("json" . "application/json")
    ("dwl" . "application/dw")
    ("csv" . "text/csv"))
  "Mime types for DataWeave runner to use when reading input.")

(defun dw-extension-to-mime (extn)
  "Return the MIME content type of the file extensions EXTN."
  (or
   (cdr (assoc (downcase extn) dw-mime-extensions))
   (mailcap-extension-to-mime extn)))

(defconst dw-output-modes
  '(("text/plain" . text-mode)
    ("application/java" . java-mode)
    ("application/json" . js-mode)
    ("text/json" . js-mode)
    ("application/xml" . xml-mode)
    ("text/xml" . xml-mode)
    ("application/dw" . dw-mode)
    ("text/dw" . dw-mode)
    ("application/csv" .csv-mode)
    ("text/csv" . csv-mode)
    ;; XXX: How to handle non-text output like xlsx?
    ;; ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" . )
    ;; ("application/xlsx" . )
    ))

(defun dw-output-type ()
  "Returns the DataWeave script output mime type."
  (save-excursion
    (save-match-data
      (goto-char (point-min))
      (re-search-forward "^\\s-*%output +\\(\\w+/\\w+\\)" nil t)
      (match-string-no-properties 1))))


;; The following are set in the output buffer
(defvar-local dw-runner-temp-files nil
  "Temporary files to be deleted when runner completes.")

(defvar-local dw-runner-error nil
  "Raw error message from WeaveRunner.")

(defvar-local dw-runner-script-buffer nil
  "Buffer with Dataweave script being run.")

(defvar-local dw-runner-output-mode nil
  "Mode to apply to output buffer.")

(defvar-local dw-runner-missing-input nil
  "Holds the name of the missing input variable.")

(defvar-local dw-runner-script-file nil)

(defun dw-eval-dwim ()
  "Evaluate something near point as Dataweave script."
  (interactive)
  (cond ((region-active-p) (dw-run (region-beginning) (region-end)))
        ((memq (char-after) '(?\( ?\{))
         (save-excursion
           (forward-char 1)
           (let ((region (bounds-of-thing-at-point 'list))) (dw-run (car region) (cdr region)))))
        ((memq (char-before) '(?\) ?\)))
         (save-excursion
           (forward-sexp -1)
           (forward-char 1)
           (let ((region (bounds-of-thing-at-point 'list))) (dw-run (car region) (cdr region)))))
        (t
         (dw-run (line-beginning-position) (line-end-position)))))

(defun dw-run (&optional body-start body-end)
  "Evaluate DataWeave script.
BODY-START and BODY-END idicate what to use as the script body (i.e. the expression). When invoked interactive if the region is active it is used."
  (interactive
   (when (region-active-p)
     (list (region-beginning) (region-end))))

  (unless (buffer-live-p dw-runner-output-buffer)
    (setq dw-runner-output-buffer (get-buffer-create (concat (buffer-name) " *Output*"))))

  (with-current-buffer dw-runner-output-buffer
    (ignore-errors (mapc 'delete-file dw-runner-temp-files))
    (when (get-buffer-process (current-buffer))
      (kill-process))
    (setq buffer-read-only nil)
    (fundamental-mode)
    (setq dw-runner-old-point (point))
    (erase-buffer))

  (let* ((script-buffer (current-buffer))
         (output-mode (or (cdr (assoc-string (dw-output-type) dw-output-modes)) 'fundamental-mode))
         (temp-script
          (dw--runner-write-script dw-runner-input-files body-start body-end))
         (input-files (mapcar 'dw--runner-write-input dw-runner-input-files)))

    (dw--start-runner-process temp-script input-files dw-runner-output-buffer
                              'dw--run-process-filter 'dw--run-process-sentinel)

    (with-current-buffer dw-runner-output-buffer
      (rename-buffer (concat (buffer-name script-buffer) " *Output*") t)
      (setq dw-runner-script-buffer script-buffer)
      (setq dw-runner-temp-files (push temp-script dw-runner-temp-files))
      (setq dw-runner-output-mode output-mode)
      (setq dw-runner-script-file temp-script)
      (setq dw-runner-input-files input-files))
    (display-buffer dw-runner-output-buffer)))

(defun dw--runner-write-input (input)
  "If the file is open in a modified buffer, gets the auto-save file name or asks users to save.
INPUT and result is a cons cell (var . file-name)."
  (if-let ((buffer (find-buffer-visiting (cdr input) 'buffer-modified-p)))
      (with-current-buffer buffer
        (cond (buffer-auto-save-file-name
               (do-auto-save t t)
               (cons (car input) buffer-auto-save-file-name))
              ((yes-or-no-p (format "Save \"%s\"? " (buffer-file-name)))
               (basic-save-buffer nil)
               input)))
    input))

(defun dw--run-process-sentinel (proc event)
  "Process sentinal for dw-run."
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      ;;re-run it?
      (if-let ((var (and (file-exists-p dw-runner-script-file)
                         dw-runner-missing-input))
               (file (condition-case err
                         (read-file-name (format  "`%s' file: " var))
                       (quit nil))))
          (progn
            (with-current-buffer dw-runner-script-buffer (dw-set-input var file))
            (push (dw--runner-write-input (cons var file)) dw-runner-input-files)
            (setq dw-runner-missing-input nil)
            (setq dw-runner-error nil)
            (erase-buffer)
            (let ((script-file dw-runner-script-file))
              (with-temp-buffer
                (insert (format "%%input %s %s\n" var (dw-extension-to-mime (file-name-extension file))))
                ;; preserve script body especially
                (insert-file-contents script-file)
                (write-region nil nil script-file)))
            (ignore (dw--start-runner-process dw-runner-script-file
                                              dw-runner-input-files
                                              (current-buffer)
                                              'dw--run-process-filter
                                              'dw--run-process-sentinel)))
        ;; clean-up
        (ignore-errors (mapc 'delete-file dw-runner-temp-files)
                       (setq dw-runner-temp-files nil))
        (cond
         ((string-equal event "finished\n")
          (if (functionp dw-runner-output-mode)
              (apply dw-runner-output-mode '())
            (message "`%s' is not installed." (symbol-name dw-runner-output-mode)))
          (goto-char (or dw-runner-old-point (point-min))))

         ;; ((string-match event "^failed with code \\(.*\\)\n"))
         ;; ((and dw-runner-error (string-equal event "exited abnormally with code 1\n")) t)
         ;; ignore
         (t (message "%s: %s" (process-name proc) (string-trim-right event))))
        (mapc (lambda (window) (set-window-point window (point))) (get-buffer-window-list))))))

(defun dw--run-process-filter (proc string)
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      (if dw-runner-error (setq dw-runner-error (concat dw-runner-error string))
        (internal-default-process-filter proc string)
        (save-excursion
          (goto-char (point-min))
          (mapc (lambda (window) (set-window-point window (point))) (get-buffer-window-list))
          (cond
           ((re-search-forward "com.mulesoft.weave.engine.ast.variables.InvalidVariableNameException: There is no variable named '\\(.*\\)'" nil t)
            (let ((var (match-string-no-properties 1)))
              (setq buffer-read-only nil)
              (setq dw-runner-error (buffer-string))
              (erase-buffer)
              (insert "No file set for input variable '" var "'.\n\n"
                      "Use `dw-set-input' in the dw-mode buffer to specify a file.\n")
              (setq-local dw-runner-missing-input var)))))))))

(defun dw--start-runner-process (weave-file-path inputs output-buffer &optional filter sentinal)
  ;; [-input <name> <value>]* [-debug]? [-output <path>]? <weave file path>
  (unless (file-readable-p weave-file-path) (error "No such file: %s" weave-file-path))
  (make-process :name "WeaveRunner"
                :buffer output-buffer
                :command (nconc
                          (list "java" "-cp" (dw--runner-classpath) "com.mulesoft.weave.WeaveRunner")
                          (apply 'nconc (mapcar (lambda (input) (list "-input" (car input) (cdr input))) inputs))
                          (list weave-file-path))
                :sentinel sentinal
                :filter filter))

(defun dw--runner-write-script (inputs &optional body-start body-end)
  "Write current buffer to a temporary file with INPUTS prepended as directives. Returns file.
Optional arguments BODY-START and BODY-END speicfy a region of a buffer to write as the body, or if BODY-START is a string it is written as the body; header section of current buffer is always written."
  ;; TODO: check for existing input directives
  (let ((file (make-temp-file (concat (file-name-sans-extension (buffer-file-name)) "-") nil (file-name-extension (buffer-file-name) t)))
        (header-end (save-excursion (goto-char (point-min)) (re-search-forward dw--rx-body-begin (point-max) t) (point))))
    (when inputs
      (write-region
       (mapconcat (lambda (input) (format "%%input %s %s\n" (car input) (dw-extension-to-mime (file-name-extension (cdr input))))) inputs "")
       nil file 'append 'ignore))

    (if header-end
        (write-region (point-min) header-end file 'append 'ignore)
      (write-region "%dw 1.0\n---\n" nil file 'append 'ignore))

    (write-region (or body-start header-end) (or body-end (point-max)) file 'append 'ignore)
    file))


;;** Auto-Run

(defvar-local dw-auto-run-timer nil)

(define-minor-mode dw-auto-run-mode ()
  "Toggle auto run."
  :group 'dataweave
  :lighter " Auto-Run"

  (if dw-auto-run-mode
      (progn
        (setq dw-auto-run-timer (run-with-idle-timer dw-auto-run-idle nil 'dw--run-with (current-buffer)))
        (add-hook 'after-change-functions 'dw--auto-run-changed nil t))
    (remove-hook 'after-change-functions 'dw--auto-run-changed t)
    (cancel-timer dw-auto-run-timer)
    (setq dw-auto-run-timer nil)))

(defun dw--auto-run-changed (start end old-len)
  (if (and dw-auto-run-mode dw-auto-run-timer (timer--triggered dw-auto-run-timer))
      (timer-activate-when-idle dw-auto-run-timer)))

(defun dw--run-with (buffer)
  (with-current-buffer buffer (dw-run)))



;;* fly-check
(require 'flycheck)

(defun dw-flycheck-parse-with-patterns (output checker buffer)
  ;; (flycheck-parse-with-patterns output checker buffer)
  (let ((errors (flycheck-parse-with-patterns output checker buffer)))
    (dolist (err errors errors)
      (unless (flycheck-error-line err)
        (setf (flycheck-error-line err) 1)
        (setf (flycheck-error-column err) 1)))))

(flycheck-define-checker dataweave
  "A Dataweave syntax checker using com.mulesoft.weave.WeaveRunner."
  :command ("java" "-cp" (eval (dw--runner-classpath)) "com.mulesoft.weave.WeaveRunner" source)
  :error-patterns ((error "Unexpected end of input, " (message) "(line " line ", column " column ")" line-end)
                   (error "Invalid input \"" (*? any) "\", " (message) "(line " line ", column " column ")" line-end )
                   (error "Exception in thread \"main\" " (message) "(line " line ", column " column ")" line-end)
                   (error "Exception in thread \"main\" " (message)))
  ;; :error-parser dw-flycheck-parse-with-patterns
  :modes dw-mode
  :verify list)


(provide 'dw-mode)
;;; dw-mode.el ends here
